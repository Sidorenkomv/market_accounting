package com.kata.model.document;

import com.kata.enums.TypeOfContract;
import com.kata.model.Yurlizo;
import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;

import javax.persistence.Entity;
import javax.persistence.ManyToOne;

@Entity
@Getter
@Setter
@NoArgsConstructor
@AllArgsConstructor
@Builder
public class Contract extends Document {

    @ManyToOne
    private Yurlizo organization;

    private TypeOfContract typeOfContract;

    private Integer code;

    private Double contractAmount;

    private Float award;

}