package com.kata.model.document;

import com.fasterxml.jackson.annotation.JsonManagedReference;
import com.kata.model.Address;
import com.kata.model.Product;
import com.kata.model.Project;
import com.kata.model.Yurlizo;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;

import javax.persistence.Entity;
import javax.persistence.ManyToMany;
import javax.persistence.ManyToOne;
import javax.persistence.OneToMany;
import javax.persistence.OneToOne;
import javax.persistence.Table;
import java.time.LocalDateTime;
import java.util.List;

@Entity
@Getter
@Setter
@NoArgsConstructor
@Table(name = "orders")
public class Order extends Document {

    @ManyToOne
    private Yurlizo organization;

    private LocalDateTime dateOfAcceptance;

    @OneToOne
    private Contract contract;

    @ManyToOne
    private Project project;

    @ManyToOne
    private Address address;

    private boolean conducted;

    private boolean reserve;

    private boolean expectation;

    private Integer externalCode;

    @ManyToMany
    private List<Product> products;

    @OneToMany
    @JsonManagedReference
    private List<Task> tasks;

    @OneToMany
    private List<File> files;

    private boolean isBuying;

}
