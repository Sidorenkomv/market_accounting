package com.kata.model;

import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;
import lombok.ToString;

import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;

@Entity
@Getter
@Setter
@NoArgsConstructor
@AllArgsConstructor
@ToString
@Builder
public class TypeOfBarcode {

    @Id
    @GeneratedValue(strategy = GenerationType.AUTO)
    private Long idTypeOfBarcode;

    private String nameTypeOfBarcode;

    private int numberOfCharacters;

    private String characterSequence;

}
