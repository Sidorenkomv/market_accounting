package com.kata.model;

import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;

import javax.persistence.CollectionTable;
import javax.persistence.Column;
import javax.persistence.ElementCollection;
import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.MapKeyColumn;
import javax.persistence.NamedAttributeNode;
import javax.persistence.NamedEntityGraph;
import javax.persistence.OneToMany;
import java.math.BigDecimal;
import java.time.LocalDateTime;
import java.util.Map;
import java.util.Set;

@NamedEntityGraph(name = "product_graph", attributeNodes = {
        @NamedAttributeNode(value = "productIdAndQuantitySetsMap"),
        @NamedAttributeNode(value = "priceNameAndValueMap"),
        @NamedAttributeNode(value = "imagesLinks"),
        @NamedAttributeNode(value = "groupOfProducts"),
        @NamedAttributeNode(value = "country"),
        @NamedAttributeNode(value = "supplier"),
        @NamedAttributeNode(value = "unitOfMeasurement"),
        @NamedAttributeNode(value = "vat"),
        @NamedAttributeNode(value = "warehouseIdMinQuantityMap"),
        @NamedAttributeNode(value = "packaging"),
        @NamedAttributeNode(value = "typeOfAccounting"),
        @NamedAttributeNode(value = "productType"),
        @NamedAttributeNode(value = "taxationSystem"),
        @NamedAttributeNode(value = "barcodes"),
        @NamedAttributeNode(value = "signOfTheSubjectOfCalculation"),
        @NamedAttributeNode(value = "lastEditedBy")})

@Entity
@Getter
@Setter
@NoArgsConstructor
@AllArgsConstructor
@Builder
public class Product {

    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    private Long id;

    private String name;

    private Boolean isService;

    private Boolean isSet;
    @ElementCollection
    @CollectionTable(name = "product_sets_map", joinColumns = {@JoinColumn(name = "product_set_id")})
    @MapKeyColumn(name = "product_id")
    @Column(name = "quantity")
    private Map<Long, Integer> productIdAndQuantitySetsMap;

    private BigDecimal minPrice;
    private BigDecimal purchasePrice;

    @ElementCollection
    @CollectionTable(name = "product_prices_map", joinColumns = {@JoinColumn(name = "product_id")})
    @MapKeyColumn(name = "price_name")
    @Column(name = "price_value")
    private Map<String, BigDecimal> priceNameAndValueMap;
    private Boolean isDiscountsForRetailSalesProhibited;

    @ElementCollection
    @CollectionTable(name = "product_images_links", joinColumns = {@JoinColumn(name = "product_id")})
    private Set<String> imagesLinks;

    private String description;

    @ManyToOne(fetch = FetchType.LAZY)
    private GroupOfProducts groupOfProducts;

    @ManyToOne(fetch = FetchType.LAZY)
    private Country country;

    @ManyToOne(fetch = FetchType.LAZY)
    private Counterparty supplier;

    private String manufacturersCode;

    private String uniqueCode;

    private String externalCode;

    @ManyToOne(fetch = FetchType.LAZY)
    private Unit unitOfMeasurement;

    private Integer weight;

    private Integer volume;

    @ManyToOne(fetch = FetchType.LAZY)
    private Vat vat;

    private Boolean isMinQuantitySummarizedInAllWarehouses;
    private Boolean isMinQuantitySameForAllWarehouses;
    private Integer minimalQuantityAtStock;

    private Boolean isCustomForEveryWarehouse;
    @ElementCollection
    @CollectionTable(name = "product_warehouse_id_min_quantity_map", joinColumns = @JoinColumn(name = "product_id"))
    @MapKeyColumn(name = "warehouse_id")
    @Column(name = "min_quantity")
    private Map<Long, Integer> warehouseIdMinQuantityMap;

    @ManyToOne(fetch = FetchType.LAZY)
    private Packaging packaging;

    @ManyToOne(fetch = FetchType.LAZY)
    private TypeOfAccounting typeOfAccounting;

    @ManyToOne(fetch = FetchType.LAZY)
    private ProductType productType;

    private Boolean isTraceable;

    @OneToMany
    @JoinColumn(name = "product_id")
    private Set<Barcode> barcodes;

    @ManyToOne(fetch = FetchType.LAZY)
    private TaxationSystem taxationSystem;

    @ManyToOne(fetch = FetchType.LAZY)
    private SignOfTheSubjectOfCalculation signOfTheSubjectOfCalculation;

    private LocalDateTime lastEdited;
    @ManyToOne(fetch = FetchType.LAZY)
    private Employee lastEditedBy;
    private Boolean isRemoved;

}
