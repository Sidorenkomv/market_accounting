package com.kata.dto;

import com.kata.model.Employee;
import com.kata.model.TaxationSystem;
import lombok.Getter;
import lombok.Setter;

import java.time.LocalDateTime;

@Getter
@Setter
public class GroupOfProductsDTO {

    private Long idGroup;

    private boolean isRemoved;

    private String nameGroup;

    private String descriptionGroup;

    private String codeGroup;

    private String externalCodeGroup;

    private VatResponseDto vat;  //НДС

    private TaxationSystem taxationSystem;  //система налогооблажения

    private LocalDateTime lastEdited;

    private Employee lastEditedBy;

}
