package com.kata.dto;


import lombok.Getter;
import lombok.Setter;

@Getter
@Setter
public class YurlizoDto {

    private Long id;

    private String name;

    private String type;

    private int inn;

    private int kpp;

    private int ogrn;

    private int okpo;

    private String yuridichadres;

}
