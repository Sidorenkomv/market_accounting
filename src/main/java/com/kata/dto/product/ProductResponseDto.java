package com.kata.dto.product;

import com.kata.dto.BarcodeDTO;
import com.kata.dto.CounterpartyDto;
import com.kata.dto.CountryDTO;
import com.kata.dto.EmployeeDTO;
import com.kata.dto.GroupOfProductsDTO;
import com.kata.dto.PackagingDTO;
import com.kata.dto.TaxationSystemDTO;
import com.kata.dto.TypeOfAccountingDto;
import com.kata.dto.UnitResponseDto;
import com.kata.dto.VatResponseDto;
import com.kata.model.ProductType;
import com.kata.model.SignOfTheSubjectOfCalculation;
import com.kata.model.TypeOfAccounting;
import lombok.Getter;
import lombok.Setter;

import java.math.BigDecimal;
import java.time.LocalDateTime;
import java.util.Map;
import java.util.Set;

@Getter
@Setter
public class ProductResponseDto {

    private Long id;

    private Boolean isService;

    private Boolean isSet;
    private Map<Long, Integer> productIdAndQuantitySetsMap;

    private BigDecimal minPrice;
    private BigDecimal purchasePrice;

    private Map<String, BigDecimal> priceNameAndValueMap;
    private Boolean isDiscountsForRetailSalesProhibited;

    private Set<String> imagesLinks;

    private String description;

    private GroupOfProductsDTO groupOfProducts;

    private CountryDTO country;

    private CounterpartyDto supplier;

    private String manufacturersCode;

    private String uniqueCode;

    private String externalCode;

    private UnitResponseDto unitOfMeasurement;

    private Integer weight;

    private Integer volume;

    private VatResponseDto vat;

    private Boolean isMinQuantitySummarizedInAllWarehouses;
    private Boolean isMinQuantitySameForAllWarehouses;
    private Integer minimalQuantityAtStock;

    private Boolean isCustomForEveryWarehouse;
    private Map<Long, Integer> warehouseIdMinQuantityMap;

    private PackagingDTO packaging;

    private TypeOfAccountingDto typeOfAccounting;

    private ProductType productType;

    private Boolean isTraceable;

    private Set<BarcodeDTO> barcodes;

    private TaxationSystemDTO taxationSystem;

    private SignOfTheSubjectOfCalculation signOfTheSubjectOfCalculation;

    private LocalDateTime lastEdited;
    private EmployeeDTO lastEditedBy;
    private Boolean isRemoved;
}
