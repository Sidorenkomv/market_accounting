package com.kata.dto.document;

import com.kata.model.document.Document;
import com.kata.model.Organization;
import com.kata.model.Product;
import com.kata.model.Project;
import com.kata.model.document.Contract;
import com.kata.model.document.File;
import com.kata.model.document.Task;
import com.kata.model.warehouse.Warehouse;
import lombok.Getter;
import lombok.Setter;

import java.time.LocalDate;
import java.time.LocalDateTime;
import java.util.List;

@Getter
@Setter
public class InvoiceDTO extends DocumentDTO{
    private Boolean isConducted;
    private Boolean isItBuying;
    private Integer paidFor;
    private Integer shipped;
    private Integer accepted;
    private Boolean isSent;
    private Boolean isPrinted;
    private Organization organization;
    private LocalDate planPaymentDate;
    private String incomingNumber;
    private LocalDateTime dateOfIncomingNumber;
    private Warehouse warehouse;
    private Contract contract;
    private Project project;
    private List<Document> relatedDocuments;
    private List<Product> productList;
    private Boolean vat;
    private Boolean vatInPrise;
    private Double interimResult;
    private Double total;
    private List<Task> taskList;
    private List<File> fileList;
}