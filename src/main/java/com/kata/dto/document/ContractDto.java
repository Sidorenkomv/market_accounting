package com.kata.dto.document;

import com.kata.enums.TypeOfContract;
import com.kata.model.Yurlizo;
import lombok.Getter;
import lombok.Setter;

@Getter
@Setter
public class ContractDto extends DocumentDTO {

    private Yurlizo organization;

    private TypeOfContract typeOfContract;

    private Integer code;

    private Double contractAmount;

    private Float award;
}
