package com.kata.dto;

import lombok.Getter;
import lombok.Setter;

/**
 * @ In the name of Allah, most gracious and most merciful! 02.12.2022
 */
@Getter
@Setter
public class ProductTypeResponseDTO {
    private Long id;
    private String name;
}