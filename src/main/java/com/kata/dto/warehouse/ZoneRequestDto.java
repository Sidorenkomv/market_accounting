package com.kata.dto.warehouse;

import lombok.Getter;
import lombok.Setter;

@Getter
@Setter
public class ZoneRequestDto {

    private String zoneName;

}
