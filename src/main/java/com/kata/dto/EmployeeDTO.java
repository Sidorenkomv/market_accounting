package com.kata.dto;

import com.kata.model.Department;
import com.kata.model.IpAddress;
import com.kata.model.IpNetwork;
import com.kata.model.Notifications;
import com.kata.model.Role;
import lombok.Getter;
import lombok.Setter;

import java.util.List;

@Getter
@Setter
public class EmployeeDTO {
    private Long id;
    private String firstName;
    private String middleName;
    private String lastName;
    private String telephone;
    private String post;
    private String individualTaxNumber;
    private String description;
    private String login;
    private String email;
    private Department department;
    private Role role;
    private List<IpAddress> accessFromAddresses;
    private List<IpNetwork> accessFromNetwork;
    private Notifications notifications;
}
