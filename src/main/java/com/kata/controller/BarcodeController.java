package com.kata.controller;

import com.kata.dto.BarcodeDTO;
import com.kata.service.BarcodeService;
import io.swagger.annotations.Api;
import io.swagger.annotations.ApiOperation;
import io.swagger.annotations.ApiParam;
import io.swagger.annotations.ApiResponse;
import io.swagger.annotations.ApiResponses;
import lombok.RequiredArgsConstructor;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.DeleteMapping;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PatchMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import java.util.List;

@RestController
@RequiredArgsConstructor
@RequestMapping("/api/goods/barcode")
@Api(value = "Controller creating a product barcode", tags = "Controller creating a product barcode")
public class BarcodeController {

    private final BarcodeService barcodeService;


    @ApiOperation(value = "Получение всех штрихкодов",
            notes = "Возвращает объекты со всеми штрихкодами")
    @ApiResponses(value = {
            @ApiResponse(code = 200, message = "Все штрихкоды получены")
    })
    @GetMapping
    public ResponseEntity<List<BarcodeDTO>> getAllBarcode() {
        return new ResponseEntity<>(barcodeService.findAll(), HttpStatus.OK);
    }


    @ApiOperation(value = "Получение штрихкода товара",
            notes = "Принимает id штрихкода из URL и возвращает объект штрихкода по id")
    @ApiResponses(value = {@ApiResponse(code = 200, message = "Штрихкод товара получен"),
            @ApiResponse(code = 404, message = "Штрихкод товара с таким id не найден")})
    @GetMapping("/{id}")
    public ResponseEntity<BarcodeDTO> getBarcodeById(@PathVariable("id") @ApiParam(name = "id", value = "id штрихкода товара", example = "1") Long idBarcode) {
        return new ResponseEntity<>(barcodeService.findById(idBarcode), HttpStatus.OK);
    }

    @ApiOperation(value = "Создание штрихкода товара",
            notes = "Принимает объект штрихкода товара")
    @ApiResponses(value = {@ApiResponse(code = 200, message = "Штрихкод товара создан"),
            @ApiResponse(code = 406, message = "Такой штрихкод существует")})
    @PostMapping
    public ResponseEntity<HttpStatus> createBarcode(@RequestBody @ApiParam(name = "Объект", value = "Объект штрихкода для создания") BarcodeDTO barcodeDTO) {
        barcodeService.save(barcodeDTO);
        return ResponseEntity.ok(HttpStatus.OK);
    }

    @ApiOperation(value = "Редактирование штрихкода товара", notes = "Принимает id из URL и объект штрихкода")
    @ApiResponses(value = {@ApiResponse(code = 200, message = "Штрихкод товара изменён"),
            @ApiResponse(code = 406, message = "Такой штрихкод существует")})
    @PatchMapping("/{id}")
    ResponseEntity<HttpStatus> updateBarcode(@PathVariable("id") @ApiParam(name = "id", value = "id штрихкода товара") Long idBarcode,
                                             @RequestBody @ApiParam(name = "Штихкод товара", value = "Объект штрихкода для редактирования")
                                             BarcodeDTO barcodeDTO){
        return ResponseEntity.ok(HttpStatus.OK);
    }

    @ApiOperation(value = "Удаление штрихкода", notes = "Принимаем id штрихкода из URL и удаляем штрихкод по id")
    @ApiResponses(value = {@ApiResponse(code = 200, message = "Штрихкод товара удалён"),
    @ApiResponse(code = 404, message = "Штрихкод с таким id не найден")})
    @DeleteMapping("/{id}")
    public ResponseEntity<HttpStatus> deleteBarcodeById(@PathVariable("id") @ApiParam(name = "id", value = "id штрихкода товара для удаления",
            example = "1")Long idBarcode) {
        barcodeService.deleteById(idBarcode);
        return ResponseEntity.ok(HttpStatus.OK);
    }

}
