package com.kata.controller;

import com.kata.dto.PackagingDTO;
import com.kata.service.PackagingService;
import io.swagger.annotations.Api;
import io.swagger.annotations.ApiOperation;
import io.swagger.annotations.ApiParam;
import io.swagger.annotations.ApiResponse;
import io.swagger.annotations.ApiResponses;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.DeleteMapping;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PatchMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;
import java.util.List;

@RestController
@RequestMapping("/api/packaging")
@Api(value = "/packaging", tags = "Фасовка")
public class PackagingRestController {

    private final PackagingService packagingService;

    @Autowired
    public PackagingRestController(PackagingService packagingService) {
        this.packagingService = packagingService;
    }

    @ApiOperation(value = "Получить все фасовки", notes = "Возвращает объекты всех васовок")
    @ApiResponses(value = {
            @ApiResponse(code = 200, message = "Все фасовки получены")
    })
    @GetMapping()
    public ResponseEntity<List<PackagingDTO>> getPackaging() {
        return new ResponseEntity<>(packagingService.index(), HttpStatus.OK);
    }

    @ApiOperation(value = "Получить выбранную фасовку", notes = "принимает id фасовки и возвращает объект")
    @ApiResponses(value = {
            @ApiResponse(code = 200, message = "Фасовка получена"),
            @ApiResponse(code = 404, message = "Фасовка с таким id не найдена")
    })
    @GetMapping("/{id}")
    public ResponseEntity<PackagingDTO> getPackagingById(@PathVariable("id") int id) {
        return new ResponseEntity<>(packagingService.showPackaging(id), HttpStatus.OK);
    }

    @ApiOperation(value = "Редактирование фасовки", notes = "Принимает id из URL и фасовку.")
    @ApiResponses(value = {
            @ApiResponse(code = 200, message = "Фасовка изменена"),
            @ApiResponse(code = 406, message = "Такая фасовка уже существует")
    })
    @PatchMapping("/{id}")
    public ResponseEntity<HttpStatus> updatePackaging(@PathVariable("id") @ApiParam(name = "id"
            , value = "Фасовка") Integer id, @RequestBody @ApiParam(name = "Фасовка"
            , value = "Фасовка для редактирования") PackagingDTO packagingDTO) {

        packagingService.updatePackaging(id, packagingDTO);
        return ResponseEntity.ok(HttpStatus.OK);
    }

    @ApiOperation(value = "Создание фасовки", notes = "Принимает объект фасовка. Поле id генерируется автоматически")
    @ApiResponses(value = {
            @ApiResponse(code = 200, message = "Фасовка создана"),
            @ApiResponse(code = 406, message = "Такая фасовка уже существует")
    })
    @PostMapping()
    public ResponseEntity<HttpStatus> createPackaging(@RequestBody @ApiParam(name = "Объект"
            , value = "Объект валюта для создания") PackagingDTO packagingDTO) {
        packagingService.savePackaging(packagingDTO);
        return ResponseEntity.ok(HttpStatus.OK);
    }

    @ApiOperation(value = "Удаление фасовки", notes = "Принимает id фасовки из URL и удаляет фасовку по id")
    @ApiResponses(value = {
            @ApiResponse(code = 200, message = "Фасовка удалена"),
            @ApiResponse(code = 404, message = "Фасовка с таким id не найдена")
    })
    @DeleteMapping("/{id}")
    public ResponseEntity<HttpStatus> deletePackaging(@PathVariable("id") int id) {
        packagingService.deletePackaging(id);
        return ResponseEntity.ok(HttpStatus.OK);
    }
}
