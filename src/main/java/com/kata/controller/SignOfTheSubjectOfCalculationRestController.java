package com.kata.controller;


import com.kata.model.SignOfTheSubjectOfCalculation;
import com.kata.service.SignOfTheSubjectOfCalculationService;
import io.swagger.annotations.Api;
import io.swagger.annotations.ApiOperation;
import io.swagger.annotations.ApiResponse;
import io.swagger.annotations.ApiResponses;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.DeleteMapping;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.PutMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import java.util.Optional;

@RestController
@RequestMapping("/api/catalog/signof")
@Api(value = "/signof", tags = "ПризнакПредметаРасчёта")
public class SignOfTheSubjectOfCalculationRestController {

    private final SignOfTheSubjectOfCalculationService signOfTheSubjectOfCalculationService;


    public SignOfTheSubjectOfCalculationRestController(
            SignOfTheSubjectOfCalculationService signOfTheSubjectOfCalculationService) {
        this.signOfTheSubjectOfCalculationService = signOfTheSubjectOfCalculationService;
    }


    @GetMapping
    @ApiOperation(value = "Предоставление сведений о всех ПредметахРасчёта приложения")
    @ApiResponses(value = {
            @ApiResponse(code = 200, message = "Сведения о ПредметахРасчёта получены"),
            @ApiResponse(code = 404, message = "Введены неверные параметры")})
    public Iterable<SignOfTheSubjectOfCalculation> getAllSignOfTheSubjectOfCalculations() {
        return signOfTheSubjectOfCalculationService.getSignOfTheSubjectOfCalculations();
    }


    @GetMapping("{id}")
    @ApiOperation(value = "Предоставление сведений о ПредметеРасчёта по Id")
    @ApiResponses(value = {
            @ApiResponse(code = 200, message = "Сведения о ПредметеРасчёта получены"),
            @ApiResponse(code = 404, message = "ПредметРасчёта с таким Id не найден")})
    public Optional<SignOfTheSubjectOfCalculation> getSignOfTheSubjectOfCalculationById(
            @PathVariable("id") Long id) {
        return  signOfTheSubjectOfCalculationService.getSignOfTheSubjectOfCalculationById(id);
    }


    @GetMapping("artikul/{artikul}")
    @ApiOperation(value = "Предоставление сведений о ПредметеРасчёта по номеру Артикла")
    @ApiResponses(value = {
            @ApiResponse(code = 200, message = "Сведения о ПредметеРасчёта получены"),
            @ApiResponse(code = 404, message = "ПредметРасчёта с таким Артиклум не найден")})
    public Iterable<SignOfTheSubjectOfCalculation> getSignOfTheSubjectOfCalculationByArtikul(
            @PathVariable("artikul") int artikul) {
        return  signOfTheSubjectOfCalculationService.getSignOfTheSubjectOfCalculationByArtikul(
                artikul);
    }

    @GetMapping("name/{name}")
    @ApiOperation(value = "Предоставление сведений о ПредметеРасчёта по Названию")
    @ApiResponses(value = {
            @ApiResponse(code = 200, message = "Сведения о ПредметеРасчёта получены"),
            @ApiResponse(code = 404, message = "ПредметРасчёта с таким Названием не найден")})
    public Iterable<SignOfTheSubjectOfCalculation> getSignOfTheSubjectOfCalculationByName(
            @PathVariable("name") String name) {
        return  signOfTheSubjectOfCalculationService.getSignOfTheSubjectOfCalculationByName(name);
    }



    @PostMapping
    @ApiOperation(value = "Создание объекта ПредметРасчёта")
    @ApiResponses(value = {
            @ApiResponse(code = 200, message = "ПредметРасчёта создан"),
            @ApiResponse(code = 404, message = "Введены неверные параметры")})
    public SignOfTheSubjectOfCalculation createSignOfTheSubjectOfCalculation(
            @RequestBody SignOfTheSubjectOfCalculation signOfTheSubjectOfCalculation) {
        return signOfTheSubjectOfCalculationService.createSignOfTheSubjectOfCalculation(
                signOfTheSubjectOfCalculation);
    }

    @PutMapping("{id}")
    @ApiOperation(value = "Обновление сведений о ПредметеРасчёта по предоставленному Id")
    @ApiResponses(value = {
            @ApiResponse(code = 200, message = "Сведения обновлены"),
            @ApiResponse(code = 404, message = "Введены неверные параметры")})
    public void updateSignof(@PathVariable("id") Long id, @RequestBody SignOfTheSubjectOfCalculation
            signOfTheSubjectOfCalculation) {
        signOfTheSubjectOfCalculationService.updateSignOfTheSubjectOfCalculation(
                id, signOfTheSubjectOfCalculation);
    }

    @DeleteMapping("{id}")
    @ApiOperation(value = "Удаление сведений о ПредметеРасчёта по предоставленному Id")
    @ApiResponses(value = {
            @ApiResponse(code = 200, message = "ПредмерРасчёта удален"),
            @ApiResponse(code = 404, message = "Введены неверные параметры")})
    public ResponseEntity<HttpStatus> deleteSignOfTheSubjectOfCalculationById(
            @PathVariable("id") Long id) {

        signOfTheSubjectOfCalculationService.deleteSignOfTheSubjectOfCalculationById(id);
        return ResponseEntity.ok(HttpStatus.OK);
    }




}
