package com.kata.controller;

import com.kata.dto.LegalDetailsDto;
import com.kata.mapper.LegalDetailsMapper;
import com.kata.model.LegalDetails;
import com.kata.service.LegalDetailsService;
import io.swagger.annotations.Api;
import io.swagger.annotations.ApiOperation;
import io.swagger.annotations.ApiParam;
import io.swagger.annotations.ApiResponse;
import io.swagger.annotations.ApiResponses;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.DeleteMapping;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PatchMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import java.util.List;

@RestController
@RequestMapping("/api/catalog/legalDetails")
@Api(value = "Controller Legal Details", description = "Сontroller responsible for legal expertise ")
public class LegalDetailsRestController {

    private final LegalDetailsService legalDetailsService;


    public LegalDetailsRestController(LegalDetailsService legalDetailsService) {
        this.legalDetailsService = legalDetailsService;
    }
    @ApiOperation(value = "Получение юр.реквизитов",
            notes = "Принимает id юр.реквизитов URL и врзвращает объект юр.реквизитов по id")
    @ApiResponses(value = {
            @ApiResponse(code = 200, message = "Юр.реквизиты получена"),
            @ApiResponse(code = 404, message = "Юр.реквизиты с таким id не найдена")
    })
    @GetMapping("/{id}")
    public ResponseEntity<LegalDetailsDto> getLegalDetailsById(@PathVariable("id") @ApiParam(name = "id", value = "Id юр.реквизитов") Long id) {
        return new ResponseEntity<>(LegalDetailsMapper.INSTANCE.toDto(legalDetailsService.findById(id)), HttpStatus.OK);
    }
    @GetMapping
    @ApiOperation(value = "Получение всех юр.реквизитов",
            notes = "Принимает всех юр.реквизиты")
    @ApiResponses(value = {
            @ApiResponse(code = 200, message = "Все юр.реквизиты получены"),
            @ApiResponse(code = 404, message = "ВСе юр.реквизиты id не найдены")
    })
    public ResponseEntity<List<LegalDetails>> getAllLegalDetails() {
        return new ResponseEntity<>(LegalDetailsMapper.INSTANCE.toDtoList(legalDetailsService.findAll()), HttpStatus.OK);
    }

    @PostMapping
    @ApiOperation(value = "Создание юр.реквизитов",
            notes = "Помечает на создание юр.реквизитов")
    @ApiResponses(value = {
            @ApiResponse(code = 200, message = "Юр.реквизиты созданы"),
            @ApiResponse(code = 406, message = "Запись с такими юр.реквизитами уже существует")
    })
    public ResponseEntity<LegalDetailsDto> createNewLegalDetails(@RequestBody @ApiParam(name = "Объект", value = "Объект юр.реквизитов для создания") LegalDetailsDto legalDetailsDto ) {
        legalDetailsService.create(legalDetailsDto);
        return new ResponseEntity<>(legalDetailsDto, HttpStatus.CREATED);
    }

    @PatchMapping("/{id}")
    @ApiResponses(value = {
            @ApiResponse(code = 200, message = "Юр.реквизиты обновлены"),
            @ApiResponse(code = 404, message = "Юр.реквизиты с таким id не найдены")
    })
    @ApiOperation( value = "Обновляет юр.реквизиты",
            notes = "Помечает на обновление юр.реквизиты")
    public ResponseEntity<LegalDetails> updateLegalDetails(@PathVariable("id") @ApiParam(name = "id", value = "Id юр.реквизитов")  Long id, @RequestBody @ApiParam(name = "Юр.реквизиты", value = "Объект юр.реквизиты для редактирования") LegalDetailsDto legalDetailsDto) {
        legalDetailsService.update(id, legalDetailsDto);
        return new ResponseEntity<>(HttpStatus.OK);
    }

    @DeleteMapping("/{id}")
    @ApiOperation(value = "Удаляет юр.реквизиты",
            notes = "Помечает на удаление юр.реквизиты")
    @ApiResponses(value = {
            @ApiResponse(code = 200, message = "Юр.реквизиты созданы"),
            @ApiResponse(code = 406, message = "Запись с такими юр.реквизитами уже существует")
    })
    public ResponseEntity<Void> deleteLegalDetails(@PathVariable("id") @ApiParam(name = "id", value = "Id юр.реквизитов для удаления") Long id) {
        legalDetailsService.delete(id);
        return ResponseEntity.ok().build();
    }


}
