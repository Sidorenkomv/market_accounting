package com.kata.controller;


import com.kata.model.Yurlizo;
import com.kata.service.YurlizoService;
import io.swagger.annotations.Api;
import io.swagger.annotations.ApiOperation;
import io.swagger.annotations.ApiResponse;
import io.swagger.annotations.ApiResponses;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.DeleteMapping;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.PutMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import java.util.Optional;

@RestController
@RequestMapping("/api/catalog/yurliza/")
@Api(value = "/yurliza", tags = "Юрлица")
public class YurlizoRestController {


    private final YurlizoService yurlizoService;

    @Autowired
    public YurlizoRestController(YurlizoService yurlizoService) {
        this.yurlizoService = yurlizoService;
    }

    @GetMapping
    @ApiOperation(value = "Предоставление сведений о всех Юрлицах приложения")
    @ApiResponses(value = {
            @ApiResponse(code = 200, message = "Сведения о Юрлицах получены"),
            @ApiResponse(code = 404, message = "Введены неверные параметры")})
    public Iterable<Yurlizo> getAllYurliza() {
        return yurlizoService.getYurliza();
    }

    @GetMapping("{id}")
    @ApiOperation(value = "Предоставление сведений о Юрлице по Id")
    @ApiResponses(value = {
            @ApiResponse(code = 200, message = "Сведения о Юрлице получены"),
            @ApiResponse(code = 404, message = "Юрлицо с таким Id не найдено")})
    public Optional<Yurlizo> getYurlizoById(@PathVariable("id") long yurlizoId) {
        return yurlizoService.getYurlizoById(yurlizoId);
    }


    @GetMapping("inn/{inn}")
    @ApiOperation(value = "Предоставление сведений о Юрлице по номеру ИНН")
    @ApiResponses(value = {
            @ApiResponse(code = 200, message = "Сведения о Юрлице получены"),
            @ApiResponse(code = 404, message = "Юрлицо с таким ИНН не найдено")})
    public Iterable<Yurlizo> getYurlizoByInn(@PathVariable("inn") int inn) {
        return yurlizoService.getYurlizaByInn(inn);
    }

    @GetMapping("name/{name}")
    @ApiOperation(value = "Предоставление сведений о Юрлице по Названию")
    @ApiResponses(value = {
            @ApiResponse(code = 200, message = "Сведения о Юрлице получены"),
            @ApiResponse(code = 404, message = "Юрлицо с таким Названием не найдено")})
    public Iterable<Yurlizo> getYurlizoByName(@PathVariable("name") String name) {
        return yurlizoService.getYurlizaByName(name);
    }


    @PostMapping
    @ApiOperation(value = "Создание объекта Юрлицо")
    @ApiResponses(value = {
            @ApiResponse(code = 200, message = "Юрлицо создано"),
            @ApiResponse(code = 404, message = "Введены неверные параметры")})
    public Yurlizo createYurlizo(@RequestBody Yurlizo yurlizo) {
        return yurlizoService.createYurlizo(yurlizo);
    }

    @PutMapping("{id}")
    @ApiOperation(value = "Обновление сведений о Юрлице по предоставленному Id")
    @ApiResponses(value = {
            @ApiResponse(code = 200, message = "Сведения обновлены"),
            @ApiResponse(code = 404, message = "Введены неверные параметры")})
    public void updateYurlizo(@PathVariable("id") long yurlizoId, @RequestBody Yurlizo yurlizo) {
        yurlizoService.updateYurlizo(yurlizoId, yurlizo);
    }

    @DeleteMapping("{id}")
    @ApiOperation(value = "Удаление сведений о Юрлице по предоставленному Id")
    @ApiResponses(value = {
            @ApiResponse(code = 200, message = "Юрлицо удалено"),
            @ApiResponse(code = 404, message = "Введены неверные параметры")})
    public ResponseEntity<HttpStatus> deleteYurlizoById(@PathVariable("id") long yurlizoId) {

        yurlizoService.deleteYurlizoById(yurlizoId);
        return ResponseEntity.ok(HttpStatus.OK);
    }


    @DeleteMapping
    @ApiOperation(value = "Удаление всех Юрлиц из приложения")
    @ApiResponses(value = {
            @ApiResponse(code = 200, message = "Юрлица удалены"),
            @ApiResponse(code = 404, message = "Введены неверные параметры")})
    public ResponseEntity<HttpStatus> deleteYurliza() {
        yurlizoService.deleteYurliza();
        return ResponseEntity.ok(HttpStatus.OK);
    }
}




