package com.kata.controller;

import com.kata.dto.CurrencyDTO;
import com.kata.service.CurrencyService;
import io.swagger.annotations.Api;
import io.swagger.annotations.ApiOperation;
import io.swagger.annotations.ApiParam;
import io.swagger.annotations.ApiResponse;
import io.swagger.annotations.ApiResponses;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.DeleteMapping;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PatchMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;
import java.util.List;

@RestController
@RequestMapping("/api/currency")
@Api(value = "/currency", tags = "Валюта")
public class CurrencyRestController {

    private final CurrencyService currencyService;

    @Autowired
    public CurrencyRestController(CurrencyService currencyService) {
        this.currencyService = currencyService;
    }

    @ApiOperation(value = "Получить все валюты", notes = "Возвращает объекты всех валют")
    @ApiResponses(value = {
            @ApiResponse(code = 200, message = "Все валюты получены")
    })
    @GetMapping()
    public ResponseEntity<List<CurrencyDTO>> getCurrencies() {
        return new ResponseEntity<>(currencyService.index(), HttpStatus.OK);
    }

    @ApiOperation(value = "Получить выбранную валюту", notes = "принимает id валюты и возвращает объект")
    @ApiResponses(value = {
            @ApiResponse(code = 200, message = "Валюта получена"),
            @ApiResponse(code = 404, message = "Валюта с таким id не найдена")
    })
    @GetMapping("/{id}")
    public ResponseEntity<CurrencyDTO> getCurrencyById(@PathVariable("id") int id) {
        return new ResponseEntity<>(currencyService.showCurrency(id), HttpStatus.OK);
    }

    @ApiOperation(value = "Редактирование валюты", notes = "Принимает id из URL и валюту.")
    @ApiResponses(value = {
            @ApiResponse(code = 200, message = "Валюта изменена"),
            @ApiResponse(code = 406, message = "Такая валюта уже существует")
    })
    @PatchMapping("/{id}")
    public ResponseEntity<HttpStatus> updateCurrency(@PathVariable("id") @ApiParam(name = "id"
            , value = "Валюта") Integer id, @RequestBody @ApiParam(name = "Валюта"
            , value = "Валюта для редактирования") CurrencyDTO currencyDTO) {

        currencyService.updateCurrency(id, currencyDTO);
        return ResponseEntity.ok(HttpStatus.OK);
    }

    @ApiOperation(value = "Создание валюты", notes = "Принимает объект валюта. Поле id генерируется автоматически")
    @ApiResponses(value = {
            @ApiResponse(code = 200, message = "Валюта создана"),
            @ApiResponse(code = 406, message = "Такая валюта уже существует")
    })
    @PostMapping()
    public ResponseEntity<HttpStatus> createCurrency(@RequestBody @ApiParam(name = "Объект"
            , value = "Объект валюта для создания") CurrencyDTO currencyDTO) {
        currencyService.saveCurrency(currencyDTO);
        return ResponseEntity.ok(HttpStatus.OK);
    }

    @ApiOperation(value = "Удаление валюты", notes = "Принимает id валюты из URL и удаляет валюту по id")
    @ApiResponses(value = {
            @ApiResponse(code = 200, message = "Валюта удалена"),
            @ApiResponse(code = 404, message = "Валюта с таким id не найдена")
    })
    @DeleteMapping("/{id}")
    public ResponseEntity<HttpStatus> deleteUser(@PathVariable("id") int id) {
        currencyService.deleteCurrency(id);
        return ResponseEntity.ok(HttpStatus.OK);
    }
}
