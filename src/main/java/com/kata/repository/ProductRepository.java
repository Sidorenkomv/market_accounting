package com.kata.repository;

import com.kata.model.Product;
import org.springframework.data.jpa.repository.EntityGraph;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Modifying;
import org.springframework.data.jpa.repository.Query;

import java.util.List;
import java.util.Optional;

public interface ProductRepository extends JpaRepository<Product, Long> {

    @EntityGraph(value = "product_graph")
    @Query("SELECT p FROM Product p")
    List<Product> findAllTypes();

    @EntityGraph(value = "product_graph")
    @Query("SELECT p FROM Product p WHERE p.isService = false AND p.isSet = false")
    List<Product> findAllProducts();

    @EntityGraph(value = "product_graph")
    @Query("SELECT p FROM Product p WHERE p.isService = true")
    List<Product> findAllServices();

    @EntityGraph(value = "product_graph")
    @Query("SELECT p FROM Product p WHERE p.isSet = true")
    List<Product> findAllSets();

    @EntityGraph(value = "product_graph")
    @Query("SELECT p FROM Product p WHERE p.id = :id")
    Optional<Product> findProductById(Long id);

    @Modifying
    @Query("UPDATE Product SET isRemoved = true WHERE id = :id")
    void markAsRemoved(Long id);
}
