package com.kata.repository.document;

import com.kata.model.document.Order;
import lombok.NonNull;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Modifying;
import org.springframework.data.jpa.repository.Query;
import org.springframework.stereotype.Repository;

import java.util.List;

@Repository
public interface OrderRepository extends JpaRepository<Order, Long> {
    @Query("SELECT o FROM Order o LEFT JOIN FETCH o.lastEditedBy WHERE o.isRemoved = false")
    List<Order> findAllByRemovedFalse();

    @Modifying
    @Query("UPDATE Order SET isRemoved = true WHERE id=:id")
    void markAsRemoved(Long id);

    @Query("SELECT o FROM Order o LEFT JOIN FETCH o.lastEditedBy WHERE o.id=:id ")
    @NonNull Order getById(@NonNull Long id);

    @Query(value = "SELECT max (number) FROM Order ")
    Long getMaxNumber();

}
