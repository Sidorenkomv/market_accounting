package com.kata.repository;

import com.kata.model.ProductType;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Modifying;
import org.springframework.data.jpa.repository.Query;
import org.springframework.stereotype.Repository;

import java.util.List;

/**
 * @ In the name of Allah, most gracious and most merciful! 02.12.2022
 */
@Repository
public interface ProductTypeRepository extends JpaRepository<ProductType, Long> {
    @Query("SELECT p FROM ProductType p WHERE p.isRemoved = false")
    List<ProductType> findAllNotRemoved();

    @Query("SELECT p FROM ProductType p WHERE p.id = :id AND p.isRemoved = false")
    ProductType findProductTypeByIdIfNotRemoved(Long id);

    @Modifying
    @Query("UPDATE ProductType SET isRemoved = true WHERE id = :id")
    void markProductTypeAsRemoved(Long id);
}
