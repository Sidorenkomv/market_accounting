package com.kata.repository;

import com.kata.model.Address;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Modifying;
import org.springframework.data.jpa.repository.Query;

import java.util.List;
import java.util.Optional;

public interface AddressRepository extends JpaRepository<Address, Long> {

    @Query("SELECT a FROM Address a LEFT JOIN FETCH a.country")
    List<Address> findAllAddresses();

    @Query("SELECT a FROM Address a LEFT JOIN FETCH a.country WHERE a.id = :id")
    Optional<Address> findAddressById(Long id);

    @Modifying
    @Query("UPDATE Address SET isRemoved = true WHERE id = :id")
    void markAddressAsRemoved(Long id);
}
