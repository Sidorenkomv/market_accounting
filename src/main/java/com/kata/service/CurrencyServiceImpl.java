package com.kata.service;

import com.kata.dto.CurrencyDTO;
import com.kata.mapper.CurrencyMapper;
import com.kata.model.Currency;
import com.kata.repository.CurrencyRepository;
import lombok.extern.slf4j.Slf4j;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;
import java.text.SimpleDateFormat;
import java.util.Date;
import java.util.List;

@Service
@Transactional
@Slf4j
public class CurrencyServiceImpl implements CurrencyService {

    private final CurrencyRepository currencyRepository;

    private final SimpleDateFormat formatter = new SimpleDateFormat("yyyy-MM-dd HH:mm:ss z");

    @Autowired
    public CurrencyServiceImpl(CurrencyRepository currencyRepository) {
        this.currencyRepository = currencyRepository;
    }

    public List<CurrencyDTO> index() {
        log.info("All Currencies was found");
        return CurrencyMapper.INSTANCE.modelsToDTOs(currencyRepository.findAllNotRemoved());
    }

    public CurrencyDTO showCurrency(int id) {
        log.info("Currency with id = {} was found", currencyRepository.getReferenceById(id));
        return CurrencyMapper.INSTANCE.modelToDto(currencyRepository.findCurrencyByIdIfNotRemoved(id));
    }

    public void saveCurrency(CurrencyDTO currencyDTO) {
        Currency currency = CurrencyMapper.INSTANCE.dtoToModel(currencyDTO);
        Date date = new Date(System.currentTimeMillis());
        currency.setTimeWhenWasChanged(formatter.format(date));
        currency.setRemoved(false);
        currencyRepository.save(currency);
        log.info("Currency was saved: {}", currency);
    }

    public void updateCurrency(int id, CurrencyDTO currencyDTO) {
        Currency fromDto = CurrencyMapper.INSTANCE.dtoToModel(currencyDTO);
        Currency newCurrency = currencyRepository.findCurrencyByIdIfNotRemoved(id);
        newCurrency.setAccountingCurrency(fromDto.isAccountingCurrency());
        newCurrency.setShortName(fromDto.getShortName());
        newCurrency.setFullName(fromDto.getFullName());
        newCurrency.setShortName(fromDto.getShortName());
        newCurrency.setDigitalCode(fromDto.getDigitalCode());
        newCurrency.setAlphabeticCode(fromDto.getAlphabeticCode());
        Date date = new Date(System.currentTimeMillis());
        newCurrency.setTimeWhenWasChanged(formatter.format(date));
        currencyRepository.save(newCurrency);
        log.info("Currency was updated: {}", newCurrency);
    }

    public void deleteCurrency(int id) {
        currencyRepository.markCurrencyAsRemoved(id);
        log.info("Currency with id = {} was marked as removed", id);
    }
}
