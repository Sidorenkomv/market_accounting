package com.kata.service;

import com.kata.model.IpNetwork;

import java.util.List;

public interface IpNetworkService {

    List<IpNetwork> findAllIpNetworks();
}
