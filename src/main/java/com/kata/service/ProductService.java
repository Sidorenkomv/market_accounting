package com.kata.service;

import com.kata.dto.product.ProductRequestDto;
import com.kata.dto.product.ProductResponseDto;

import java.util.List;

public interface ProductService {

    List<ProductResponseDto> findAll();

    List<ProductResponseDto> findAllProducts();

    List<ProductResponseDto> findAllServices();

    List<ProductResponseDto> findAllSets();

    ProductResponseDto findById(Long id);

    void save(ProductRequestDto productRequestDto);

    void update(Long id, ProductRequestDto productRequestDto);

    void delete(Long id);

}
