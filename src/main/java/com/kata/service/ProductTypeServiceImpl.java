package com.kata.service;

import com.kata.dto.ProductTypeRequestDTO;
import com.kata.dto.ProductTypeResponseDTO;
import com.kata.mapper.ProductTypeMapper;
import com.kata.model.ProductType;
import com.kata.repository.ProductTypeRepository;
import lombok.RequiredArgsConstructor;
import lombok.extern.slf4j.Slf4j;
import org.springframework.stereotype.Service;

import javax.transaction.Transactional;
import java.util.List;

/**
 * @ In the name of Allah, most gracious and most merciful! 02.12.2022
 */
@Service
@RequiredArgsConstructor
@Slf4j
public class ProductTypeServiceImpl implements ProductTypeService {
    private final ProductTypeRepository productTypeRepository;
    private final ProductTypeMapper productTypeMapper;

    @Override
    public List<ProductTypeResponseDTO> getAllProductTypes() {
        log.info("Fetch all product types");
        return productTypeMapper.toDtoList(productTypeRepository.findAllNotRemoved());
    }

    @Override
    public ProductTypeResponseDTO getProductType(Long id) {
        log.info("{} was found", productTypeRepository.findProductTypeByIdIfNotRemoved(id));
        return productTypeMapper.toDto(productTypeRepository.findProductTypeByIdIfNotRemoved(id));
    }

    @Override
    @Transactional
    public void addProductType(ProductTypeRequestDTO productTypeRequestDTO) {
        ProductType productType = productTypeMapper.toEntity(productTypeRequestDTO);
        productType.setRemoved(false);
        productType.setId(productType.getId());
        productTypeRepository.save(productType);
        log.info("{} was saved", productType);
    }

    @Override
    @Transactional
    public void updateProductType(Long id, ProductTypeRequestDTO productTypeRequestDTO) {
        ProductType fromDTO = productTypeMapper.toEntity(productTypeRequestDTO);
        ProductType newPT = productTypeRepository.findProductTypeByIdIfNotRemoved(id);
        newPT.setName(fromDTO.getName());
        newPT.setRemoved(false);
        productTypeRepository.save(newPT);
        log.info("Product type: {} with id: {} was updated", newPT, id);
    }

    @Override
    @Transactional
    public void deleteProductType(Long id) {
        productTypeRepository.markProductTypeAsRemoved(id);
        log.info("Product type with id: {} was marked as removed", id);
    }
}