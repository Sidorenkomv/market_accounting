package com.kata.service.warehouse;

import com.kata.dto.warehouse.WarehouseRequestDto;
import com.kata.dto.warehouse.WarehouseResponseDto;
import com.kata.mapper.warehouse.WarehouseMapper;
import com.kata.model.warehouse.Warehouse;
import com.kata.model.warehouse.Zone;
import com.kata.repository.warehouse.WarehouseRepository;
import lombok.AllArgsConstructor;
import lombok.extern.slf4j.Slf4j;
import org.springframework.stereotype.Service;

import javax.transaction.Transactional;
import java.util.List;
import java.util.Set;

import static java.util.Objects.nonNull;

@Slf4j
@Service
@AllArgsConstructor
public class WarehouseServiceImpl implements WarehouseService {

    private final WarehouseRepository repository;
    private final WarehouseMapper mapper;
    private final ZoneService zoneService;
    private final CellService cellService;

    @Override
    public List<WarehouseResponseDto> findAll() {
        log.info("All warehouses have been received");

        return mapper.toDtoList(repository.findAllNotRemoved());
    }

    @Override
    public WarehouseResponseDto findById(Long id) {
        log.info("Warehouse with id = {} was received", id);

        return mapper.toDto(repository.findById(id).orElse(null));
    }

    @Override
    @Transactional
    public Warehouse save(WarehouseRequestDto dto) {
        Warehouse warehouse = mapper.toEntity(dto);
        if (nonNull(warehouse)) {
            setParentIfExists(dto, warehouse);

            warehouse.setZones(zoneService.getZonesForSave(dto));
            warehouse.setCells(cellService.getFilledCells(dto.getCells()));

            repository.save(warehouse);

            log.info("Warehouse was created: {}", dto);
        }
        return warehouse;
    }

    @Override
    @Transactional
    public void update(Long id, WarehouseRequestDto dto) {
        Set<Zone> zones = zoneService.getZonesForUpdate(id, dto);

        Warehouse warehouse = mapper.toEntity(dto);
        warehouse.setZones(zones);

        setParentIfExists(dto, warehouse);
        warehouse.setCells(cellService.getFilledCells(dto.getCells()));
        warehouse.setId(id);

        repository.save(warehouse);

        log.info("Warehouse was updated: {}", dto);
    }

    @Override
    @Transactional
    public void deleteById(Long id) {
        repository.markAsRemoved(id);

        log.info("Warehouse with id = {} was marked as removed", id);
    }

    private void setParentIfExists(WarehouseRequestDto dto, Warehouse warehouse) {
        if (nonNull(dto.getParentId())) {
            warehouse.setParent(repository.findById(dto.getParentId()).orElse(null));
        }
    }
}
