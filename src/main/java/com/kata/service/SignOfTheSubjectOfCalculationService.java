package com.kata.service;

import com.kata.model.SignOfTheSubjectOfCalculation;

import java.util.Optional;

public interface SignOfTheSubjectOfCalculationService {


    SignOfTheSubjectOfCalculation createSignOfTheSubjectOfCalculation(
            SignOfTheSubjectOfCalculation signOfTheSubjectOfCalculation);

    Optional<SignOfTheSubjectOfCalculation> getSignOfTheSubjectOfCalculationById(Long id);


    Iterable<SignOfTheSubjectOfCalculation>
    getSignOfTheSubjectOfCalculationByName(String name);


    Iterable<SignOfTheSubjectOfCalculation>
    getSignOfTheSubjectOfCalculationByArtikul(int artikul);


    Iterable<SignOfTheSubjectOfCalculation> getSignOfTheSubjectOfCalculations();

    void updateSignOfTheSubjectOfCalculation(Long id, SignOfTheSubjectOfCalculation signOfTheSubjectOfCalculation);



    void deleteSignOfTheSubjectOfCalculationById(Long id);



}
