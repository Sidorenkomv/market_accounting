package com.kata.service;

import com.kata.dto.TaxationSystemDTO;
import com.kata.mapper.TaxationSystemMapper;
import com.kata.model.TaxationSystem;
import com.kata.repository.TaxationSystemRepository;
import lombok.RequiredArgsConstructor;
import lombok.extern.slf4j.Slf4j;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import java.util.List;

@Service
@RequiredArgsConstructor
@Slf4j
public class TaxationSystemServiceImpl implements TaxationSystemService{
    private final TaxationSystemRepository taxationSystemRepository;

    @Override
    public List<TaxationSystemDTO> findAll() {

        log.info("All Taxation systems was found");
        return TaxationSystemMapper.INSTANCE.toDtoList(taxationSystemRepository.findAllNotRemoved());
    }

    @Override
    public TaxationSystemDTO findById(Long id) {

        log.info("Taxation system with id = {} was found", id);
        return TaxationSystemMapper.INSTANCE.toDto(taxationSystemRepository.findTaxationSystemByIdIfNotRemoved(id));
    }

    @Override
    @Transactional
    public void save(TaxationSystemDTO taxationSystemDTO) {

        TaxationSystem taxationSystem = TaxationSystemMapper.INSTANCE.toEntity(taxationSystemDTO);
        taxationSystem.setIsRemoved(false);
        taxationSystemRepository.save(taxationSystem);
        log.info("Taxation system was saved: {}", taxationSystemDTO);
    }

    @Override
    @Transactional
    public void update(Long id, TaxationSystemDTO taxationSystemDTO) {
        TaxationSystem fromDto = TaxationSystemMapper.INSTANCE.toEntity(taxationSystemDTO);
        TaxationSystem newTaxationSystem = taxationSystemRepository.findTaxationSystemByIdIfNotRemoved(id);
        newTaxationSystem.setName(fromDto.getName());
        newTaxationSystem.setIsRemoved(false);
        taxationSystemRepository.save(newTaxationSystem);
        log.info("Taxation system was updated: {}", newTaxationSystem);
    }

    @Override
    @Transactional
    public void deleteById(Long id) {
        taxationSystemRepository.markTaxationSystemAsRemoved(id);
        log.info("Taxation system with id = {} was marked as removed", id);
    }
}
