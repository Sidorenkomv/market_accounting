package com.kata.service.document;

import com.kata.dto.document.InvoiceDTO;

import java.util.List;

public interface InvoiceService {

    List<InvoiceDTO> findAll();

    InvoiceDTO findById(Long id);

    void save(InvoiceDTO invoiceDTO);

    void update(Long id, InvoiceDTO invoiceDTO);

    void deleteById(Long id);
}
