package com.kata.service.document;

import com.kata.dto.document.OrderRequestDto;
import com.kata.dto.document.OrderResponseDto;

import java.util.List;

public interface OrderService {
    List<OrderResponseDto> findAll();

    OrderResponseDto findById(Long id);

    void save(OrderRequestDto dto);

    void update(Long id, OrderRequestDto dto);

    void deleteById(Long id);
}
