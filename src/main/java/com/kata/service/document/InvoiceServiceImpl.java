package com.kata.service.document;

import com.kata.dto.document.InvoiceDTO;
import com.kata.mapper.document.InvoiceMapper;
import com.kata.model.document.Invoice;
import com.kata.repository.document.InvoiceRepository;
import lombok.RequiredArgsConstructor;
import lombok.extern.slf4j.Slf4j;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import java.util.List;

@Service
@RequiredArgsConstructor
@Slf4j
public class InvoiceServiceImpl implements InvoiceService{
    private final InvoiceRepository invoiceRepository;

    @Override
    public List<InvoiceDTO> findAll() {
        log.info("All Invoices was found");
        return InvoiceMapper.INSTANCE.toDtoList(invoiceRepository.findAllNotRemoved());
    }

    @Override
    public InvoiceDTO findById(Long id) {

        log.info("Invoice with id = {} was found", id);
        return InvoiceMapper.INSTANCE.toDto(invoiceRepository.findInvoiceByIdIfNotRemoved(id));
    }

    @Override
    @Transactional
    public void save(InvoiceDTO invoiceDTO) {
        Invoice invoice = InvoiceMapper.INSTANCE.toEntity(invoiceDTO);

        invoice.setIsRemoved(false);
        invoiceRepository.save(invoice);
        log.info("Invoice was saved: {}", invoice);
    }

    @Override
    @Transactional
    public void update(Long id, InvoiceDTO invoiceDTO) {
        Invoice fromDto = InvoiceMapper.INSTANCE.toEntity(invoiceDTO);
        Invoice newInvoice = invoiceRepository.findInvoiceByIdIfNotRemoved(id);

        newInvoice.setWhenWasCreated(fromDto.getWhenWasCreated());
        newInvoice.setStatus(fromDto.getStatus());
        newInvoice.setIsConducted(fromDto.getIsConducted());
        newInvoice.setIsItBuying(fromDto.getIsItBuying());
        newInvoice.setPaidFor(fromDto.getPaidFor());
        newInvoice.setShipped(fromDto.getShipped());
        newInvoice.setAccepted(fromDto.getAccepted());
        newInvoice.setIsSent(fromDto.getIsSent());
        newInvoice.setIsPrinted(fromDto.getIsPrinted());
        newInvoice.setOrganization(fromDto.getOrganization());
        newInvoice.setCounterparty(fromDto.getCounterparty());
        newInvoice.setPlanPaymentDate(fromDto.getPlanPaymentDate());
        newInvoice.setIncomingNumber(fromDto.getIncomingNumber());
        newInvoice.setDateOfIncomingNumber(fromDto.getDateOfIncomingNumber());
        newInvoice.setSalesChannel(fromDto.getSalesChannel());
        newInvoice.setWarehouse(fromDto.getWarehouse());
        newInvoice.setContract(fromDto.getContract());
        newInvoice.setProject(fromDto.getProject());
        newInvoice.setRelatedDocuments(fromDto.getRelatedDocuments());
        newInvoice.setProductList(fromDto.getProductList());
        newInvoice.setVat(fromDto.getVat());
        newInvoice.setVatInPrise(fromDto.getVatInPrise());
        newInvoice.setInterimResult(fromDto.getInterimResult());
        newInvoice.setTotal(fromDto.getTotal());
        newInvoice.setComment(fromDto.getComment());
        newInvoice.setTaskList(fromDto.getTaskList());
        newInvoice.setFileList(fromDto.getFileList());
        newInvoice.setIsRemoved(false);
        invoiceRepository.save(newInvoice);
        log.info("Invoice was updated: {}", newInvoice);
    }

    @Override
    @Transactional
    public void deleteById(Long id) {

        invoiceRepository.markInvoiceAsRemoved(id);
        log.info("Invoice with id = {} was marked as removed", id);
    }
}
