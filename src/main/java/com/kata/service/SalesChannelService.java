package com.kata.service;

import com.kata.dto.SalesChannelDTO;

import java.util.List;

public interface SalesChannelService {

    List<SalesChannelDTO> findAll();

    SalesChannelDTO findById(Long id);

    void save(SalesChannelDTO salesChannelDTO);

    void update(Long id, SalesChannelDTO salesChannelDTO);

    void delete(Long id);
}
