package com.kata.service;

import com.kata.dto.BarcodeDTO;
import java.util.List;

public interface BarcodeService {
    List<BarcodeDTO> findAll();

    BarcodeDTO findById(Long idBarcode);

    void save(BarcodeDTO barcodeDTO);

    void update(Long idBarcode, BarcodeDTO barcodeDTO);

    void deleteById(Long idBarcode);
}
