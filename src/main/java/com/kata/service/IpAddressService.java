package com.kata.service;

import com.kata.model.IpAddress;

import java.util.List;

public interface IpAddressService {

    List<IpAddress> findAllIpAddresses();
}
