package com.kata.service;

import com.kata.model.Notifications;
import com.kata.repository.NotificationsRepository;
import lombok.RequiredArgsConstructor;
import org.springframework.stereotype.Service;

import java.util.List;

@Service
@RequiredArgsConstructor
public class NotificationsServiceImpl implements NotificationsService{
    private final NotificationsRepository repository;

    @Override
    public List<Notifications> findAllNotifications() {
        return repository.findAll();
    }
}
