package com.kata.service;

import com.kata.model.Role;
import com.kata.repository.RoleRepository;
import lombok.RequiredArgsConstructor;
import org.springframework.stereotype.Service;

import java.util.List;

@Service
@RequiredArgsConstructor
public class RoleServiceImpl implements RoleService{
    private final RoleRepository repository;

    @Override
    public List<Role> findAllRoles() {
        return repository.findAll();
    }
}
