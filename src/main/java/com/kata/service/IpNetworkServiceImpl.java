package com.kata.service;

import com.kata.model.IpNetwork;
import com.kata.repository.IpNetworkRepository;
import lombok.RequiredArgsConstructor;
import org.springframework.stereotype.Service;

import java.util.List;

@Service
@RequiredArgsConstructor
public class IpNetworkServiceImpl implements IpNetworkService{
    private final IpNetworkRepository repository;

    @Override
    public List<IpNetwork> findAllIpNetworks() {
        return repository.findAll();
    }
}
