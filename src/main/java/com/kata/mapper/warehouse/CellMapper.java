package com.kata.mapper.warehouse;

import com.kata.dto.warehouse.CellRequestDto;
import com.kata.model.warehouse.Cell;
import org.mapstruct.Mapper;

import java.util.Set;

@Mapper(componentModel = "spring")
public interface CellMapper {

    Set<Cell> toCellSet(Set<CellRequestDto> cellDtoSet);

}
