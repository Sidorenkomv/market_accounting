package com.kata.mapper.warehouse;

import com.kata.dto.warehouse.WarehouseRequestDto;
import com.kata.dto.warehouse.WarehouseResponseDto;
import com.kata.model.warehouse.Warehouse;
import org.mapstruct.Mapper;
import org.mapstruct.Mapping;

import java.util.List;

@Mapper(componentModel = "spring")
public interface WarehouseMapper {

    @Mapping(ignore = true, target = "cells")
    Warehouse toEntity(WarehouseRequestDto dto);

    @Mapping(target="parentId", source="parent.id")
    WarehouseResponseDto toDto(Warehouse warehouse);

    List<WarehouseResponseDto> toDtoList(List<Warehouse> warehouses);

}
