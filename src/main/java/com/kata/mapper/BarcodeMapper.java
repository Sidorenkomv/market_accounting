package com.kata.mapper;

import com.kata.dto.BarcodeDTO;
import com.kata.model.Barcode;
import org.mapstruct.Mapper;
import org.mapstruct.factory.Mappers;

import java.util.List;

@Mapper
public interface BarcodeMapper {
    BarcodeMapper INSTANCE = Mappers.getMapper(BarcodeMapper.class);

    List<BarcodeDTO> toDtoList(List<Barcode> barcodes);

    BarcodeDTO toDto(Barcode barcode);

    Barcode toEntity(BarcodeDTO barcodeDTO);
}
