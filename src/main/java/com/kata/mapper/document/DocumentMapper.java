package com.kata.mapper.document;

import com.kata.dto.document.ContractDto;
import com.kata.dto.document.DocumentDTO;
import com.kata.dto.document.InvoiceDTO;
import com.kata.dto.document.OrderResponseDto;
import com.kata.model.document.Contract;
import com.kata.model.document.Document;
import com.kata.model.document.Invoice;
import com.kata.model.document.Order;
import org.mapstruct.Mapper;
import org.mapstruct.SubclassMapping;
import org.mapstruct.factory.Mappers;

import java.util.List;

@Mapper
public interface DocumentMapper {

    DocumentMapper INSTANCE = Mappers.getMapper(DocumentMapper.class);

    @SubclassMapping(source = Invoice.class, target = InvoiceDTO.class)
    @SubclassMapping(source = Order.class, target = OrderResponseDto.class)
    @SubclassMapping(source = Contract.class, target = ContractDto.class)
    DocumentDTO modelToDto(Document document);


    List<DocumentDTO> modelsToDTOs(List<Document> documents);

    @SubclassMapping(source = InvoiceDTO.class, target = Invoice.class)
    @SubclassMapping(source = OrderResponseDto.class, target = Order.class)
    @SubclassMapping(source = ContractDto.class, target = Contract.class)
    Document dtoToModel(DocumentDTO documentDTO);
}

