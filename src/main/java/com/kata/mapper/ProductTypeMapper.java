package com.kata.mapper;

import com.kata.dto.ProductTypeRequestDTO;
import com.kata.dto.ProductTypeResponseDTO;
import com.kata.model.ProductType;
import org.mapstruct.Mapper;

import java.util.List;

/**
 * @ In the name of Allah, most gracious and most merciful! 02.12.2022
 */
@Mapper(componentModel = "spring")
public interface ProductTypeMapper {
    ProductType toEntity(ProductTypeRequestDTO productTypeRequestDTO);

    ProductTypeResponseDTO toDto(ProductType productType);

    List<ProductTypeResponseDTO> toDtoList(List<ProductType> productType);
}