package com.kata.mapper;

import com.kata.dto.CurrencyDTO;
import com.kata.model.Currency;
import org.mapstruct.Mapper;
import org.mapstruct.factory.Mappers;
import java.util.List;

@Mapper(componentModel = "spring")
public interface CurrencyMapper {

    CurrencyMapper INSTANCE = Mappers.getMapper(CurrencyMapper.class);

    CurrencyDTO modelToDto(Currency country);

    List<CurrencyDTO> modelsToDTOs(List<Currency> countries);

    Currency dtoToModel(CurrencyDTO currencyDTO);
}
