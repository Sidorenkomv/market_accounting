package com.kata.config.db.initilizer;

public interface DataInitializer {

    void dataInitialization();
}
