create table cell
(
    id        bigserial not null,
    cell_name varchar(255),
    is_free   boolean   not null,
    zone_id   bigint,
    primary key (id)
);

create table zone
(
    id             bigserial not null,
    free_cells     integer,
    occupied_cells integer,
    total_cells    integer,
    zone_name      varchar(255),
    primary key (id)
);

alter table cell add
    constraint cell_zone foreign key (zone_id) references zone (id);


create table warehouse
(
    id             bigserial not null,
    code           integer,
    comment        varchar(255),
    external_code  integer,
    is_removed     boolean   not null,
    warehouse_name varchar(255),
    address_id     bigint,
    parent_id      bigint,
    primary key (id),
    constraint warehouse_address foreign key (address_id) references address (id),
    constraint warehouse_parent foreign key (parent_id) references warehouse(id)
);

create table warehouse_cells
(
    warehouse_id bigint not null,
    cells_id     bigint not null,
    primary key (warehouse_id, cells_id),
    constraint warehouse_cells foreign key (cells_id) references cell (id),
    constraint cells_warehouse foreign key (warehouse_id) references warehouse (id)
);

create table warehouse_zones
(
    warehouse_id bigint not null,
    zones_id     bigint not null,
    primary key (warehouse_id, zones_id),
    constraint warehouse_zones foreign key (zones_id) references zone (id),
    constraint zones_warehouse foreign key (warehouse_id) references warehouse (id)
)