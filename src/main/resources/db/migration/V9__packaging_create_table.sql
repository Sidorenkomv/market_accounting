create table packaging (
                           id  serial not null,
                           is_removed boolean not null,
                           packaging_type varchar(255),
                           primary key (id)
)