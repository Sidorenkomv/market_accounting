CREATE TABLE taxation_system
(
    id         BIGINT GENERATED BY DEFAULT AS IDENTITY NOT NULL,
    name       VARCHAR(255),
    is_removed BOOLEAN DEFAULT FALSE NOT NULL,
    CONSTRAINT pk_taxationsystem PRIMARY KEY (id)
);